package ipca.project.musicify

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.*
import com.google.android.material.button.MaterialButton
import ipca.project.musicify.MainActivity.Companion.fbDatabase
import ipca.project.musicify.MenuActivity.Companion.userModel
import ipca.project.musicify.models.MusicModel
import ipca.project.musicify.models.Pattern
import ipca.project.musicify.models.Track
import ipca.project.musicify.views.PatternView
import ipca.project.musicify.wrappers.MusicRender
import ipca.project.musicify.wrappers.NukeSSLCerts
import kotlinx.android.synthetic.main.activity_music_maker.*
import kotlinx.android.synthetic.main.row_track.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.sql.Timestamp

@Suppress("NAME_SHADOWING")
class MusicMaker : AppCompatActivity() {

    var tracks: MutableList<Track> = ArrayList()
    private var adapterTrack: TrackAdapter? = null

    var patterns: MutableList<Pattern> = ArrayList()
    private var adapterPattern: PatternAdapter? = null

    private var isResume = false
    private var musicPlayers: MutableList<MediaPlayer> = ArrayList()

    //Max size list
    var maxS = 0.0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_maker)

        tracks.add(Track("Drums", 60, true))
        tracks.add(Track("Guita", 60, true))

        patterns.add(Pattern("One", "Drums"))
        patterns.add(Pattern("Two", "Guitarra"))

        patterns[0].setInitFinal(1.0F, 300.0F)
        patterns[1].setInitFinal(1.0F, 300.0F)

        patterns[0].linkMusicMp3 = "https://firebasestorage.googleapis.com/v0/b/musicify-c0255.appspot.com/o/Tracks%2FTHL.mp3?alt=media&token=11b5df90-3c22-4b33-91dc-ac99676e819a"
        patterns[1].linkMusicMp3 = "https://firebasestorage.googleapis.com/v0/b/musicify-c0255.appspot.com/o/Tracks%2FTHL.mp3?alt=media&token=11b5df90-3c22-4b33-91dc-ac99676e819a"

        adapterTrack = TrackAdapter()
        listViewTracks.adapter = adapterTrack

        adapterPattern = PatternAdapter()
        listViewPattern.adapter = adapterPattern

        val view = findViewById<View>(R.id.mView)
        var start = 0.0f

        view.setOnTouchListener{ view, event ->
            when(event?.action){
                MotionEvent.ACTION_DOWN -> {
                    start = view.x - event.rawX
                    isResume = true
                }
                MotionEvent.ACTION_MOVE -> {
                    var move = event.rawX + start
                    move = move.coerceIn(view.left.toFloat(), maxS)
                    view.animate().x(move).setDuration(0).start()
                }
            }
            true
        }

        imageButtonSave.setOnClickListener {
            val dialog = Dialog(this@MusicMaker)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(true)
            dialog.setContentView(R.layout.dialog_musicshare)

            val imgButtonSave = dialog.findViewById(R.id.imageButtonSave) as MaterialButton
            val imgButtonCancel = dialog.findViewById(R.id.imageButtonCancel) as MaterialButton

            imgButtonSave.setOnClickListener {
                val musicName = dialog.findViewById<EditText>(R.id.editTextName)
                val imageUrl = dialog.findViewById<EditText>(R.id.editTextImage)
                val shared = dialog.findViewById<Switch>(R.id.switchShared)
                val boolShared = shared.isChecked

                val queue = Volley.newRequestQueue(this, HurlStack(null , NukeSSLCerts.nuke()))

                val musicRender = MusicRender(tracks, patterns)

                val url = "http://172.16.31.30:5000/musicprocessor/" + musicRender.completeObject().toString()

                doAsync {
                    val request = StringRequest(Request.Method.POST, url, Response.Listener { response ->
                            // Process the json
                            try {
                                val music = MusicModel(userModel!!.id.toString(), userModel!!.username.toString(), musicName.text.toString(), imageUrl.text.toString(), response, 0,0, boolShared, false, arrayListOf(), false, Timestamp(System.currentTimeMillis()).toString())

                                val myRefMusic = fbDatabase.database.getReference("Musicas").push()

                                myRefMusic.setValue(music).addOnSuccessListener {
                                    Toast.makeText(baseContext, "Register Done", Toast.LENGTH_SHORT).show()
                                }.addOnFailureListener{
                                    Toast.makeText(baseContext, "Register Db Not Done", Toast.LENGTH_SHORT).show()
                                }
                                uiThread {
                                    Toast.makeText(it, "Nice: $response", Toast.LENGTH_LONG).show()
                                }
                            } catch (e: Exception) {
                                uiThread {
                                    Toast.makeText(it, "Erro1: ${e.message}", Toast.LENGTH_LONG).show()
                                }
                            }

                        }, Response.ErrorListener { error ->
                            // Error in request
                            uiThread {
                                Log.d("Error2: ", error.message.toString())
                                Log.d("Error2: ", error.cause.toString())
                            }
                        })

                    request.retryPolicy = DefaultRetryPolicy(25 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
                    queue.add(request)
                    queue.start()
                }
            }

            imgButtonCancel.setOnClickListener {
                dialog.dismiss()
            }

            dialog.show()
        }

        load()
    }

    inner class TrackAdapter : BaseAdapter() {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = layoutInflater.inflate(R.layout.row_track, parent, false)

            val trackNameTextView = view.findViewById<TextView>(R.id.textViewTrackName)
            val trackVolumeSeek = view.findViewById<SeekBar>(R.id.seekBarVolume)
            val trackActiveSwitch = view.findViewById<Switch>(R.id.switchTrackActive)

            val track = tracks[position]

            trackNameTextView.text = track.name
            trackActiveSwitch.isChecked = track.active
            trackVolumeSeek.progress = track.volume

            trackActiveSwitch.setOnClickListener {
                trackActiveSwitch.isChecked = !trackActiveSwitch.isChecked
                track.active = trackActiveSwitch.isChecked
            }

            view.imageButtonDelete.setOnClickListener {
                tracks.remove(track)

                val pattern = patterns[position]

                patterns.remove(pattern)

                reloadUi()
            }

            view.imageButtonCopy.setOnClickListener {
                val pattern = Pattern("Copy: $position", "Copy")
                pattern.setInitFinal(
                    patterns[position].initialTime + 300.0F,
                    patterns[position].finalTime + 300.0F
                )

                pattern.linkMusicMp3 = patterns[position].linkMusicMp3
                patterns.add(pattern)

                val newTrack = Track(pattern.name, 100, true)

                tracks.add(newTrack)

                reloadUi()
            }

            trackVolumeSeek?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    //Code
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {
                    //Code
                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {
                    track.volume = trackVolumeSeek.progress
                    Toast.makeText(applicationContext, "Volume progress: " + trackVolumeSeek.progress.toString(), Toast.LENGTH_SHORT).show()
                }
            })

            return view
        }

        override fun getItem(position: Int): Any {
            return tracks[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getCount(): Int {
            return tracks.size
        }

    }

    inner class PatternAdapter : BaseAdapter() {
        @SuppressLint("ViewHolder")
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = layoutInflater.inflate(R.layout.row_pattern, parent, false)

            val patternView = view.findViewById<PatternView>(R.id.patternView)

            val pattern = patterns[position]

            patternView.name = pattern.name
            patternView.start = pattern.initialTime
            patternView.end = pattern.finalTime

            patternView.touchEvent = {
                pattern.setInitFinal(patternView.start, patternView.end)
            }

            return view
        }

        override fun getItem(position: Int): Any {
            return patterns[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getCount(): Int {
            return patterns.size
        }
    }

    private fun playOnTime(index: Int) {
        musicPlayers[index].start()
    }

    private fun stopAll() {
        for (i in 0 until patterns.size) {
            if(musicPlayers[i].isPlaying){
                musicPlayers[i].stop()
                musicPlayers[i].reset()
            }
        }
    }

    private fun stopOnTime(index: Int) {
        musicPlayers[index].pause()
        musicPlayers[index].stop()
    }

    private fun load() {
        doAsync {
            for (i in 0 until patterns.size) {
                try {
                    if (tracks[i].active) {
                        musicPlayers.add(i, MediaPlayer())
                        musicPlayers[i].setDataSource(patterns[i].linkMusicMp3)
                        musicPlayers[i].isLooping = true
                        musicPlayers[i].setVolume(
                            tracks[i].volume.toFloat(),
                            tracks[i].volume.toFloat()
                        )
                        musicPlayers[i].prepare()
                    } else {
                        break
                    }
                } catch (e: Exception) {
                    Toast.makeText(baseContext, e.toString(), Toast.LENGTH_SHORT).show()
                    Log.d("Load Error", e.toString())
                }
            }
        }

    }

    fun reloadUi() {
        adapterTrack = TrackAdapter()
        listViewTracks.adapter = adapterTrack

        adapterPattern = PatternAdapter()
        listViewPattern.adapter = adapterPattern

        load()
    }

    override fun onEnterAnimationComplete() {
        super.onEnterAnimationComplete()

        maxS = listViewPattern.width.toFloat()

        val mView = ObjectAnimator.ofFloat(mView, View.TRANSLATION_X,maxS)
        mView.duration = 180000
        mView.start()
        mView.pause()

        val view = findViewById<View>(R.id.mView)

        for(i in 0 until patterns.size){
            patterns[i].maxS = maxS
        }


        imageButtonPlay.setOnClickListener {

            if (!isResume) {
                mView.resume()
                mView.addUpdateListener {
                    for (i in 0 until patterns.size) {
                        if (view.x.toInt() == patterns[i].initialTime.toInt()) {
                            playOnTime(i)
                        }

                        if (view.x.toInt() == patterns[i].finalTime.toInt()) {
                            stopOnTime(i)
                        }
                    }
                }
                isResume = true
            }
            /*else {
                mView.start()
                isResume = false
            }*/
        }

        imageButtonStop.setOnClickListener {
            mView.pause()
            stopAll()
            isResume = false
        }

        imageButtonRestart.setOnClickListener{
            mView.start()
            isResume = true
        }

        imageButtonBackHome.setOnClickListener{
            //Intent
            val intent = Intent(this@MusicMaker, MenuActivity::class.java)

            //Start intent
            startActivity(intent)
        }
    }
}


