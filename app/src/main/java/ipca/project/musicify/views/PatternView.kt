package ipca.project.musicify.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

public class PatternView : View {

    val paddingConst = 50.0F

    var start = 1.0F
    var end = 300.0F
    private var center = end - start
    private var size = 0.0f
    var name : String? = null
    var touchEvent : ((tempValue: Float) -> Unit)? = null

    constructor(context: Context) : super(context){
        init()
    }

    constructor(context:Context, attr: AttributeSet) : super(context, attr){
        init()
    }

    constructor(
        context:Context,
        attr: AttributeSet,
        defStyleAttr: Int) :
            super(context, attr, defStyleAttr){
        init()
    }

    fun init(){

    }

    //On draw canvas
    @SuppressLint("DrawAllocation", "CanvasSize")
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        val paint = Paint()
        paint.color = Color.DKGRAY
        paint.strokeWidth = 4.0F
        paint.style = Paint.Style.FILL_AND_STROKE

        val paintYellow = Paint()
        paintYellow.color = Color.YELLOW
        paintYellow.strokeWidth = 4.0F
        paintYellow.style = Paint.Style.FILL_AND_STROKE

        canvas?.let {

            val rect = RectF(
                start.coerceAtLeast(1.0F),
                0.0F,
                end,
                it.height.toFloat()
            )

            val rectStart = RectF(
                start.coerceAtLeast(1.0F),
                0.0F,
                start.coerceAtLeast(1.0F)+paddingConst,
                it.height.toFloat()
            )

            val rectEnd = RectF(
                end-paddingConst,
                0.0F,
                end,
                it.height.toFloat()
            )

            it.drawRect(rect, paint)
            it.drawRect(rectStart, paintYellow)
            it.drawRect(rectEnd, paintYellow)
        }
    }

    var isTouching = false

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {

        when(event!!.action){
            MotionEvent.ACTION_DOWN ->{
                isTouching = true
            }
            MotionEvent.ACTION_UP ->{
                isTouching = false
            }
            MotionEvent.ACTION_MOVE ->{
                if (!isTouching) return false

                event.let {touch ->
                    if (touch.x > start+paddingConst && touch.x < end-paddingConst){
                        center = end - start

                        size = (center/2)

                        start = touch.x - size
                        end = touch.x + size

                    }else if(touch.x < start+paddingConst && touch.x > start){

                        center = start+paddingConst - start

                        val size = (center/2)

                        start = touch.x - size

                    }else if(touch.x > end-paddingConst && touch.x < end){
                        center = end - (end-paddingConst)

                        val size = (center/2)

                        end = touch.x + size

                    }

                }

                touchEvent!!.invoke(start)
                touchEvent!!.invoke(end)
                invalidate()
            }


        }

        return true
    }


}