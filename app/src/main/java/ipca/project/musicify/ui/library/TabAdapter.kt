package ipca.project.musicify.ui.library

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import ipca.project.musicify.ui.library.tabFragments.TabRecentCreatedMusics
import ipca.project.musicify.ui.library.tabFragments.TabRecentPlayedMusics
import ipca.project.musicify.ui.library.tabFragments.TabMostListenedMusics

class TabAdapter(fm: FragmentManager?, var numberTabs: Int) :
    FragmentStatePagerAdapter(fm!!) {
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> TabRecentCreatedMusics()
                1 -> TabRecentPlayedMusics()
                2 -> TabMostListenedMusics()
                else -> Fragment()
            }
        }

        override fun getCount(): Int {
            return numberTabs
        }
}
