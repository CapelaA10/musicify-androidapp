package ipca.project.musicify.ui.library.tabFragments

import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import ipca.project.musicify.MainActivity
import ipca.project.musicify.MenuActivity.Companion.musicModelList
import ipca.project.musicify.MenuActivity.Companion.userModel
import ipca.project.musicify.R
import ipca.project.musicify.models.MusicModel
import ipca.project.musicify.ui.library.LibraryFragment
import ipca.project.musicify.wrappers.AudioManager
import ipca.project.musicify.wrappers.GetImageFromURL
import kotlin.math.roundToInt

class TabMostListenedMusics : Fragment() {

    private var listMLMSorted : MutableList<MusicModel> = ArrayList()
    private var recyclerView: RecyclerView? = null
    private var adapter: AlbumsAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_library_tab_3, container, false)

        recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)

        listMLMSorted.addAll(userModel!!.playedMusics)
        listMLMSorted.sortByDescending { row -> row.clicks }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = AlbumsAdapter(view.context, listMLMSorted)

        val mLayoutManager : RecyclerView.LayoutManager = GridLayoutManager(view.context, 2)
        recyclerView!!.layoutManager = mLayoutManager
        recyclerView!!.addItemDecoration(LibraryFragment.GridSpacingItemDecoration(2, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10f, resources.displayMetrics).roundToInt(), true))
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = adapter

        adapter!!.notifyDataSetChanged()
    }

    inner class AlbumsAdapter(private val mContext: Context, private val albumList: List<MusicModel>) : RecyclerView.Adapter<AlbumsAdapter.MyViewHolder>() {

        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var title : TextView = view.findViewById<TextView>(R.id.title)
            var count : TextView = view.findViewById<TextView>(R.id.count)
            var thumbnail : ImageView = view.findViewById<ImageView>(R.id.thumbnail)
            var eye : MaterialButton = view.findViewById<MaterialButton>(R.id.buttonVisibility)
            var overflow : MaterialButton = view.findViewById<MaterialButton>(R.id.overflow)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_library_card, parent, false)
            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.title.text = albumList[position].name
            holder.count.text = "Clicks: ${albumList[position].clicks}"
            GetImageFromURL(holder.thumbnail).execute(albumList[position].urlImage)

            if (albumList[position].authorId != userModel!!.id) {
                holder.eye.visibility = View.INVISIBLE
                holder.overflow.visibility = View.INVISIBLE
            } else {
                if (albumList[position].private!!)
                    holder.eye.setIconResource(R.drawable.ic_visibility_off)
                else
                    holder.eye.setIconResource(R.drawable.ic_visibility)

                holder.overflow.setOnClickListener {
                    showPopupMenu(holder, albumList[position])
                }
            }

            holder.thumbnail.setOnClickListener {
                AudioManager.toolbar(context, albumList[position], holder.thumbnail)
                adapter!!.notifyDataSetChanged()
            }
        }

        /**
         * Showing popup menu when tapping on configs
         */
        private fun showPopupMenu(holder: MyViewHolder, album: MusicModel) { // inflate menu
            val popup = PopupMenu(mContext, holder.thumbnail)
            val inflater = popup.menuInflater

            inflater.inflate(R.menu.menu_library, popup.menu)


            popup.setOnMenuItemClickListener(MyMenuItemClickListener(holder, album))
            try {
                val fieldPopup = PopupMenu::class.java.getDeclaredField("mPopup")
                fieldPopup.isAccessible = true
                val menu = fieldPopup.get(popup)
                menu.javaClass.getDeclaredMethod("setForceShowIcon", Boolean::class.java).invoke(menu, true)
            } catch (e: Exception){
                e.printStackTrace()
            } finally {
                popup.show()
            }
        }

        /**
         * Click listener for popup menu items
         */
        inner class MyMenuItemClickListener(view: MyViewHolder, musicModel: MusicModel) : PopupMenu.OnMenuItemClickListener {
            var holder = view
            var album = musicModel

            override fun onMenuItemClick(menuItem: MenuItem): Boolean {
                when (menuItem.itemId) {
                    R.id.itemEdit -> {
                        Toast.makeText(mContext, "Feature Coming", Toast.LENGTH_SHORT).show()
                        return true
                    }

                    R.id.itemVisibility -> {
                        if (album.private!!) {
                            holder.eye.setIconResource(R.drawable.ic_visibility_off)
                            album.private = false
                        } else {
                            holder.eye.setIconResource(R.drawable.ic_visibility)
                            album.private = true
                        }


                        musicModelList.forEach { oldValue -> oldValue.takeIf { it.id == album.id }?.let {
                            it.private = album.private
                        }}

                        adapter!!.notifyDataSetChanged()
                        MainActivity.fbDatabase.child("Musicas").child(album.id!!).child("private").setValue(album.private)
                        return true
                    }

                    R.id.itemShare -> {
                        if (album.share!!) {
                            holder.eye.setIconResource(R.drawable.ic_visibility_off)
                            album.share = false
                            Toast.makeText(mContext, "You just remove the music from the public!", Toast.LENGTH_SHORT).show()
                        } else {
                            holder.eye.setIconResource(R.drawable.ic_visibility)
                            album.share = true
                            Toast.makeText(mContext, "You just shared the music to public!", Toast.LENGTH_SHORT).show()
                        }


                        musicModelList.forEach { oldValue -> oldValue.takeIf { it.id == album.id }?.let {
                            it.share = album.share
                        }}

                        adapter!!.notifyDataSetChanged()
                        MainActivity.fbDatabase.child("Musicas").child(album.id!!).child("share").setValue(album.share)
                        return true
                    }

                    R.id.itemDelete -> {
                        listMLMSorted.remove(album)
                        musicModelList.forEachIndexed { index, oldValue -> oldValue.takeIf { it.id == album.id }?.let {
                            musicModelList.drop(index)
                        }}

                        adapter!!.notifyDataSetChanged()
                        MainActivity.fbDatabase.child("Musicas").child(album.id!!).child("disabled").setValue(true)
                        Toast.makeText(mContext, "You deleted the music!", Toast.LENGTH_SHORT).show()
                        return true
                    }
                }
                return false
            }
        }

        override fun getItemCount(): Int {
            return albumList.size
        }

    }
}
