package ipca.project.musicify.ui.share

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.button.MaterialButton
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.MutableData
import com.google.firebase.database.Transaction
import ipca.project.musicify.MainActivity.Companion.fbDatabase
import ipca.project.musicify.MenuActivity
import ipca.project.musicify.MenuActivity.Companion.musicModelList
import ipca.project.musicify.MenuActivity.Companion.userModel
import ipca.project.musicify.R
import ipca.project.musicify.models.MusicModel
import ipca.project.musicify.wrappers.AudioManager
import ipca.project.musicify.wrappers.GetImageFromURL
import kotlinx.android.synthetic.main.fragment_share.*
import org.jetbrains.anko.imageBitmap
import java.sql.Timestamp
import kotlin.random.Random

class ShareFragment : Fragment() {

    private val musicAdapter : MusicAdapter = MusicAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_share, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        listMusicsShared.adapter = musicAdapter

        Log.d("VALUES", "Terminei as tarefas.")
    }

    inner class MusicAdapter : BaseAdapter() {
        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
            if (!musicModelList[p0].share!!) return View(context)

            val view = layoutInflater.inflate(R.layout.row_share, null)
            //view.tag = "VIEW" + ShareViewModel.musicModelList[p0].id
            Log.d("VALUES", "NOME ${musicModelList[p0].name}")

            val musicImage = view.findViewById<ImageView>(R.id.libraryMusicPicture)
            val musicName = view.findViewById<TextView>(R.id.textLibraryMusicName)
            val musicAuthor = view.findViewById<TextView>(R.id.textLibraryActions)

            val buttonLike = view.findViewById<MaterialButton>(R.id.buttonLike)
            val buttonDislike = view.findViewById<MaterialButton>(R.id.buttonDislike)

            buttonLike.tag = "BUTTON-LIKE" + musicModelList[p0].id
            buttonDislike.tag = "BUTTON-DISLIKE" + musicModelList[p0].id
            Log.d("VALUES", "ID: ${musicModelList[p0].id} button: ${buttonLike.id} TAG: ${buttonLike.tag}")

            GetImageFromURL(musicImage).execute(musicModelList[p0].urlImage)
            musicName.text = musicModelList[p0].name
            musicAuthor.text = musicModelList[p0].author

            buttonLike.text = musicModelList[p0].likes.toString()
            buttonDislike.text = musicModelList[p0].dislikes.toString()


            Log.d("VALUES", "Likes | ID: ${musicModelList[p0].id} | Condição: ${musicModelList[p0].id in userModel!!.likedMusics}")
            Log.d("VALUES-ARRAY", "Likes array: ${userModel!!.likedMusics.joinToString()}")
            if (musicModelList[p0].id in userModel!!.likedMusics) {
                buttonLike.setTextColor(resources.getColor(R.color.colorLiked))
                buttonLike.setIconTintResource(R.color.colorLiked)
            } else if (musicModelList[p0].id in userModel!!.dislikedMusics) {
                buttonDislike.setTextColor(resources.getColor(R.color.colorDisliked))
                buttonDislike.setIconTintResource(R.color.colorDisliked)
            }

            buttonLike.setOnClickListener{
                incrementValue(musicModelList[p0], "likes", userModel!!.dislikedMusics.contains(musicModelList[p0].id), userModel!!.likedMusics.contains(musicModelList[p0].id))
            }

            buttonDislike.setOnClickListener{
                incrementValue(musicModelList[p0], "dislikes", userModel!!.likedMusics.contains(musicModelList[p0].id), userModel!!.dislikedMusics.contains(musicModelList[p0].id))
            }

            view.setOnClickListener {
                AudioManager.toolbar(context, musicModelList[p0], musicImage)
            }

            return view
        }

        override fun getItem(p0: Int): Any {
            return musicModelList[p0]
        }

        override fun getItemId(p0: Int): Long {
            return 0
        }

        override fun getCount(): Int {
            return musicModelList.size
        }
    }

    fun incrementValue(musicModel: MusicModel, action: String, decrementOther: Boolean, decrementYourself: Boolean) {
        fbDatabase.child("Musicas").child(musicModel.id.toString()).runTransaction(object : Transaction.Handler {
            override fun doTransaction(currentData: MutableData) : Transaction.Result {
                Log.d("FIREBASE", "Current data: " + currentData)

                val incrementVal = if (action.equals("likes", true)) action else "dislikes"
                val decrementVal = if (action.equals("likes", true)) "dislikes" else "likes"

                if (currentData.child(incrementVal).value == null || currentData.child(if (decrementYourself) action else decrementVal).value == null) {
                    activity!!.runOnUiThread {
                        Toast.makeText(context, "Try again", Toast.LENGTH_SHORT).show()
                    }
                    return Transaction.abort()
                }
                // Default values for firebase database
                val firebaseValueDefault : HashMap<String, Int> = HashMap()
                firebaseValueDefault["value"] = 0

                val buttonLike = view!!.findViewWithTag<MaterialButton>("BUTTON-LIKE" + musicModel.id)
                val buttonDislike = view!!.findViewWithTag<MaterialButton>("BUTTON-DISLIKE" + musicModel.id)

                if (action.equals("likes")) {
                    if (userModel!!.dislikedMusics.contains(musicModel.id)) {
                        activity!!.runOnUiThread {
                            buttonDislike.setTextColor(resources.getColor(R.color.colorNoLikeOrDislike))
                            buttonDislike.setIconTintResource(R.color.colorNoLikeOrDislike)
                        }

                        musicModel.dislikes = musicModel.dislikes?.minus(1)

                        userModel!!.dislikedMusics.remove(musicModel.id)
                        fbDatabase.child("users").child(userModel!!.id.toString()).child("dislikedMusics").child(musicModel.id!!).removeValue()
                    }

                    musicModel.likes = if(decrementYourself) musicModel.likes?.minus(1) else musicModel.likes?.plus(1)

                    if (!decrementYourself) {
                        activity!!.runOnUiThread {
                            buttonLike.rippleColor = resources.getColorStateList(R.color.colorNoLikeOrDislike)
                            buttonDislike.rippleColor = resources.getColorStateList(R.color.colorDisliked)
                            buttonLike.setTextColor(resources.getColor(R.color.colorLiked))
                            buttonLike.setIconTintResource(R.color.colorLiked)
                        }

                        userModel!!.likedMusics.add(musicModel.id.toString())
                        fbDatabase.child("users").child(userModel!!.id.toString()).child("likedMusics").child(musicModel.id!!).setValue(firebaseValueDefault)
                    } else {
                        activity!!.runOnUiThread {
                            buttonLike.rippleColor = resources.getColorStateList(R.color.colorLiked)
                            buttonLike.setTextColor(resources.getColor(R.color.colorNoLikeOrDislike))
                            buttonLike.setIconTintResource(R.color.colorNoLikeOrDislike)
                        }

                        userModel!!.likedMusics.remove(musicModel.id)
                        fbDatabase.child("users").child(userModel!!.id.toString()).child("likedMusics").child(musicModel.id!!).removeValue()
                    }
                } else if (action.equals("dislikes")) {
                    if (userModel!!.likedMusics.contains(musicModel.id)) {
                        activity!!.runOnUiThread {
                            buttonLike.setTextColor(resources.getColor(R.color.colorNoLikeOrDislike))
                            buttonLike.setIconTintResource(R.color.colorNoLikeOrDislike)
                        }

                        musicModel.likes = musicModel.likes?.minus(1)

                        userModel!!.likedMusics.remove(musicModel.id)
                        fbDatabase.child("users").child(userModel!!.id.toString()).child("likedMusics").child(musicModel.id!!).removeValue()
                    }

                    musicModel.dislikes = if(decrementYourself) musicModel.dislikes?.minus(1) else musicModel.dislikes?.plus(1)

                    if (!decrementYourself) {
                        activity!!.runOnUiThread {
                            buttonDislike.rippleColor = resources.getColorStateList(R.color.colorNoLikeOrDislike)
                            buttonLike.rippleColor = resources.getColorStateList(R.color.colorLiked)
                            buttonDislike.setTextColor(resources.getColor(R.color.colorDisliked))
                            buttonDislike.setIconTintResource(R.color.colorDisliked)
                        }

                        userModel!!.dislikedMusics.add(musicModel.id.toString())
                        fbDatabase.child("users").child(userModel!!.id.toString()).child("dislikedMusics").child(musicModel.id!!).setValue(firebaseValueDefault)
                    } else {
                        activity!!.runOnUiThread {
                            buttonDislike.rippleColor = resources.getColorStateList(R.color.colorDisliked)
                            buttonDislike.setTextColor(resources.getColor(R.color.colorNoLikeOrDislike))
                            buttonDislike.setIconTintResource(R.color.colorNoLikeOrDislike)
                        }

                        userModel!!.dislikedMusics.remove(musicModel.id)
                        fbDatabase.child("users").child(userModel!!.id.toString()).child("dislikedMusics").child(musicModel.id!!).removeValue()
                    }
                }

                if (!decrementYourself) currentData.child(incrementVal).value = currentData.child(incrementVal).value as Long + 1
                if (decrementYourself || decrementOther) currentData.child(if (decrementYourself) action else decrementVal).value = currentData.child(if (decrementYourself) action else decrementVal).value as Long - 1

                activity!!.runOnUiThread {
                    buttonLike.text = currentData.child("likes").value.toString()
                    buttonDislike.text = currentData.child("dislikes").value.toString()
                }

                musicModel.likes = currentData.child("likes").value.toString().toInt()
                musicModel.dislikes = currentData.child("dislikes").value.toString().toInt()

                return Transaction.success(currentData)
            }

            override fun onComplete(databaseError: DatabaseError?, b: Boolean, dataSnapshot: DataSnapshot?) {
                if (databaseError != null) {
                    Log.d("FIREBASE", "Firebase transaction wasn't sucess!")
                } else {
                    Log.d("FIREBASE", "Firebase transaction was succeeded!")
                }
            }
        })
    }
}
