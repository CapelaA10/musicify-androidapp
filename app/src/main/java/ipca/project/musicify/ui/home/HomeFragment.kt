package ipca.project.musicify.ui.home

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.core.view.MotionEventCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ipca.project.musicify.MusicMaker
import ipca.project.musicify.R
import kotlinx.android.synthetic.main.fragment_home.*
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

class HomeFragment : Fragment() {

    var ip : String = "192.168.4.1"
    var port : Int = 2807
    var socket = DatagramSocket()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //New Music
        buttonNewMusic.setOnClickListener{

            //Intent
            val intent = Intent(requireActivity(), MusicMaker::class.java)

            //Start intent
            startActivity(intent)

        }

        buttonPlayA.setOnTouchListener { view, event ->

            ip = editTextIp.text.toString()

            when(event?.action){
                MotionEvent.ACTION_DOWN -> {
                    val messageA = "G3F100!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
                MotionEvent.ACTION_UP -> {
                    val messageA = "G3F0!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
            }
            true
        }

        buttonPlayB.setOnTouchListener { view, event ->

            ip = editTextIp.text.toString()

            when(event?.action){
                MotionEvent.ACTION_DOWN -> {
                    val messageA = "G3F200!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
                MotionEvent.ACTION_UP -> {
                    val messageA = "G3F0!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
            }
            true
        }

        buttonPlayC.setOnTouchListener { view, event ->

            ip = editTextIp.text.toString()

            when(event?.action){
                MotionEvent.ACTION_DOWN -> {
                    val messageA = "G3F300!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
                MotionEvent.ACTION_UP -> {
                    val messageA = "G3F0!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
            }
            true
        }


        buttonPlayD.setOnTouchListener { view, event ->

            ip = editTextIp.text.toString()

            when(event?.action){
                MotionEvent.ACTION_DOWN -> {
                    val messageA = "G3F400!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
                MotionEvent.ACTION_UP -> {
                    val messageA = "G3F0!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
            }
            true
        }

        buttonPlayE.setOnTouchListener { view, event ->

            ip = editTextIp.text.toString()

            when(event?.action){
                MotionEvent.ACTION_DOWN -> {
                    val messageA = "G3F500!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
                MotionEvent.ACTION_UP -> {
                    val messageA = "G3F0!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
            }
            true
        }

        buttonPlayF.setOnTouchListener { view, event ->

            ip = editTextIp.text.toString()

            when(event?.action){
                MotionEvent.ACTION_DOWN -> {
                    val messageA = "G3F600!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
                MotionEvent.ACTION_UP -> {
                    val messageA = "G3F0!"

                    val checkComplete = checkSumC(messageA)

                    sendUDP(checkComplete)
                }
            }
            true
        }
    }

    fun sendUDP(message : String) {

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()

        StrictMode.setThreadPolicy(policy)

        try {
            //Open a port to send the package
            socket = DatagramSocket()
            socket.broadcast = true

            val sendData = message.toByteArray()

            val sendPacket = DatagramPacket(sendData, sendData.size, InetAddress.getByName(ip), port)

            socket.send(sendPacket)

        } catch (e: IOException) {
            Log.e("Error", "IOException: " + e.message)
        }
    }

    fun checkSumC(data : String) : String{

        var dataSend = data
        var checkSum = 0

        val charArr = data.toCharArray()

        for (i in 0 until data.length){
            checkSum += charArr[i].toByte()
        }

        dataSend += checkSum.toString()

        return dataSend
    }

}