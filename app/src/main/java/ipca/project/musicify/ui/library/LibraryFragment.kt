package ipca.project.musicify.ui.library

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.button.MaterialButton
import com.google.android.material.tabs.TabLayout
import ipca.project.musicify.MenuActivity.Companion.musicModelList
import ipca.project.musicify.MenuActivity.Companion.userModel
import ipca.project.musicify.R

class LibraryFragment : Fragment() {

    var toolbarText : String? = "Recently musics created"
    var headerText : TextView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        userModel!!.playedMusics.forEach { oldValue ->
            musicModelList.forEach{ row -> oldValue.takeIf{ it.id == row.id }?.let {
                Log.d("VALUES", "NEW VALUE ${row.id} ${row.private}" )
                it.authorId = row.authorId
                it.name = row.name
                it.urlMusic = row.urlMusic
                it.urlImage = row.urlImage
                it.private = row.private
                it.share = row.share
                Log.d("VALUES", "OLD VALUE ${it.id} ${it.private}" )
            }}
        }

        val view : View = inflater.inflate(R.layout.fragment_library, container, false)

        view.isFocusableInTouchMode = true
        view.requestFocus()

        view.setOnKeyListener { view, keyPressed, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                if (keyPressed == KeyEvent.KEYCODE_BACK) {
                    (activity!! as AppCompatActivity).supportActionBar!!.show()
                    true
                }
            }
            false
        }

        // Inflate the layout for this fragment
        return view
    }

    override fun onDetach() {
        super.onDetach()
        (activity!! as AppCompatActivity).supportActionBar!!.show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity!! as AppCompatActivity).supportActionBar!!.hide()

        val buttonNavigation = view.findViewById<MaterialButton>(R.id.library_openNavigation)
        buttonNavigation.setOnClickListener {
            val navDrawer: DrawerLayout = activity!!.findViewById(R.id.drawer_layout)
            // If the navigation drawer is not open then open it, if its already open then close it.
            if (!navDrawer.isDrawerOpen(GravityCompat.START))
                navDrawer.openDrawer(GravityCompat.START)
            else
                navDrawer.closeDrawer(GravityCompat.END)
        }

        headerText = view.findViewById(R.id.headerTitle)

        //(activity!! as AppCompatActivity).setSupportActionBar(toolbar)
        initCollapsingToolbar(view)
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private fun initCollapsingToolbar(view: View) {
        val collapsingToolbar = view.findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar)
        collapsingToolbar.title = " "

        val appBarLayout = view.findViewById<AppBarLayout>(R.id.appbar)
        appBarLayout.setExpanded(true)

        val tabLayout = view.findViewById<TabLayout>(R.id.tablayout)
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_create))
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_hearing))
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_touch_app))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        val viewPager = view.findViewById<ViewPager>(R.id.pager)
        val adapter = TabAdapter(fragmentManager, tabLayout.tabCount)

        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                toolbarText = when (tab.position) {
                    0 -> "Recently musics created"
                    1 -> "Recently played musics"
                    2 -> "Most listened musics"
                    else -> "Bugou tudo!"
                }

                headerText!!.text = toolbarText
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(object : OnOffsetChangedListener {
            var isShow = false
            var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.title = toolbarText
                    tabLayout.visibility = View.INVISIBLE
                    isShow = true
                } else if (isShow) {
                    collapsingToolbar.title = " "
                    tabLayout.visibility = View.VISIBLE
                    isShow = false
                }
            }
        })
    }




    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    class GridSpacingItemDecoration(private val spanCount: Int, private val spacing: Int, private val includeEdge: Boolean) : ItemDecoration() {
        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) // top edge
                    outRect.top = spacing

                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)

                if (position >= spanCount)
                    outRect.top = spacing // item top
            }
        }
    }
}

/*
class LibraryFragment : Fragment() {
    companion object {
        var listRCMSorted : MutableList<MusicModel> = ArrayList()
        var listRPMSorted : MutableList<MusicModel> = ArrayList()
        var listMLSSorted : MutableList<MusicModel> = ArrayList()
    }

    val recentlyCreatedAdapter : RecentlyCreatedAdapter = RecentlyCreatedAdapter()
    val recentlyPlayedAdapter : RecentlyPlayedAdapter = RecentlyPlayedAdapter()
    val mostListenedAdapter : MostListenedAdapter = MostListenedAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_library, container, false)

        if (listRCMSorted.isEmpty()){
            ShareViewModel.musicModelList.forEach { row -> row.takeIf { it.authorId == userModel!!.id }?.let {
                listRCMSorted.add(it)
            } }
            listRCMSorted.sortByDescending { date -> date.created }
        }

        if (listRPMSorted.isEmpty()){
            listRPMSorted.addAll(userModel!!.playedMusics)
            listRPMSorted.forEach {oldValue ->
                ShareViewModel.musicModelList.forEach{row -> oldValue.takeIf{ it.id == row.id }?.let {
                    Log.d("VALUES", "NEW VALUE ${row.id} ${row.private}" )
                    it.urlImage = row.urlImage
                    it.name = row.name
                    it.private = row.private
                    Log.d("VALUES", "OLD VALUE ${it.id} ${it.private}" )
                }}
            }
            listRPMSorted.sortByDescending { date -> date.lastPlay }
        }

        if (listMLSSorted.isEmpty()){
            listMLSSorted.addAll(userModel!!.playedMusics)
            listMLSSorted.sortByDescending { row -> row.clicks }

        }
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        listRCM.adapter = recentlyCreatedAdapter
        listRPM.adapter = recentlyPlayedAdapter
        listMLS.adapter = mostListenedAdapter
    }



    inner class RecentlyCreatedAdapter : BaseAdapter() {
        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
            val view = layoutInflater.inflate(R.layout.row_library_recent, p2, false)

            val musicImage = view.findViewById<ImageView>(R.id.libraryMusicPicture)
            val musicName = view.findViewById<TextView>(R.id.textLibraryMusicName)
            val musicActions = view.findViewById<TextView>(R.id.textLibraryActions)

            val likes = view.findViewById<TextView>(R.id.textLikes)
            val dislikes = view.findViewById<TextView>(R.id.textDislikes)

            val btnEye = view.findViewById<MaterialButton>(R.id.buttonVisibility)
            val btnSettings = view.findViewById<MaterialButton>(R.id.buttonSettings)

            btnEye.visibility = View.GONE
            btnSettings.isEnabled = false
            likes.visibility = View.GONE
            dislikes.visibility = View.GONE

            GetImageFromURL(musicImage).execute(listRCMSorted[p0].urlImage)
            musicName.text = listRCMSorted[p0].name

            musicActions.text = "Created at:  ${listRCMSorted[p0].created!!.split(" ")[0]}"

            Log.d("VALUES", "PRIVATE: ${listRCMSorted[p0].id} " + listRCMSorted[p0].private)
            if (listRCMSorted[p0].private!!)
                btnSettings.setIconResource(R.drawable.ic_visibility_off)
            else
                btnSettings.setIconResource(R.drawable.ic_visibility)

            view.setOnClickListener {
                val musicIntent = Intent(context, LibrarySingleActivity::class.java)
                musicIntent.putExtra("intentToLibrarySingle", listRCMSorted as Serializable)

                startActivity(musicIntent)
            }

            return view
        }

        override fun getItem(p0: Int): Any {
            return listRCMSorted[p0]
        }

        override fun getItemId(p0: Int): Long {
            return 0
        }

        override fun getCount(): Int {
            return listRCMSorted.size
        }

    }



    inner class RecentlyPlayedAdapter : BaseAdapter() {
        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
            val view = layoutInflater.inflate(R.layout.row_library_recent, p2, false)

            val musicImage = view.findViewById<ImageView>(R.id.libraryMusicPicture)
            val musicName = view.findViewById<TextView>(R.id.textLibraryMusicName)
            val musicActions = view.findViewById<TextView>(R.id.textLibraryActions)

            val likes = view.findViewById<TextView>(R.id.textLikes)
            val dislikes = view.findViewById<TextView>(R.id.textDislikes)

            val btnEye = view.findViewById<MaterialButton>(R.id.buttonVisibility)
            val btnSettings = view.findViewById<MaterialButton>(R.id.buttonSettings)

            btnEye.visibility = View.GONE
            btnSettings.isEnabled = false
            likes.visibility = View.GONE
            dislikes.visibility = View.GONE

            GetImageFromURL(musicImage).execute(listRPMSorted[p0].urlImage)
            musicName.text = listRPMSorted[p0].name
            musicActions.text = "Last played:  ${listRPMSorted[p0].lastPlay!!.toString().split(" ")[0]}"

            Log.d("VALUES", "PRIVATE: ${listRPMSorted[p0].id} " + listRPMSorted[p0].private)
            if (listRPMSorted[p0].private!!)
                btnSettings.setIconResource(R.drawable.ic_visibility_off)
            else
                btnSettings.setIconResource(R.drawable.ic_visibility)

            view.setOnClickListener {
                val musicIntent = Intent(context, LibrarySingleActivity::class.java)
                musicIntent.putExtra("intentToLibrarySingle", listRPMSorted as Serializable)

                startActivity(musicIntent)
            }

            return view
        }

        override fun getItem(p0: Int): Any {
            return listRPMSorted[p0]
        }

        override fun getItemId(p0: Int): Long {
            return 0
        }

        override fun getCount(): Int {
            return listRPMSorted.size
        }

    }



    inner class MostListenedAdapter : BaseAdapter() {
        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
            val view = layoutInflater.inflate(R.layout.row_library_mostlistened, p2, false)

            val musicImage = view.findViewById<ImageView>(R.id.libraryMusicPicture)
            val musicName = view.findViewById<TextView>(R.id.textLibraryMusicName)
            val musicClicksAmount = view.findViewById<TextView>(R.id.textLibraryClicksAmount)

            GetImageFromURL(musicImage).execute(listMLSSorted[p0].urlImage)
            musicName.text = listMLSSorted[p0].name
            musicClicksAmount.text = listMLSSorted[p0].clicks.toString()

            view.setOnClickListener {
                val musicIntent = Intent(context, LibrarySingleActivity::class.java)
                musicIntent.putExtra("intentToLibrarySingle", listMLSSorted as Serializable)

                startActivity(musicIntent)
            }

            return view
        }

        override fun getItem(p0: Int): Any {
            return listMLSSorted[p0]
        }

        override fun getItemId(p0: Int): Long {
            return 0
        }

        override fun getCount(): Int {
            return listMLSSorted.size
        }

    }
}
*/