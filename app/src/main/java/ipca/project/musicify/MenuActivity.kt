package ipca.project.musicify

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import ipca.project.musicify.MainActivity.Companion.fbDatabase
import ipca.project.musicify.models.MusicModel
import ipca.project.musicify.models.UserModel
import ipca.project.musicify.ui.library.LibraryFragment
import kotlinx.android.synthetic.main.activity_menu.*
import java.sql.Timestamp


class MenuActivity : AppCompatActivity() {

    //App bar
    private lateinit var appBarConfiguration: AppBarConfiguration

    // Firebase Auth init and load musics
    private lateinit var auth: FirebaseAuth

    //The user model in static
    companion object {
        var userModel : UserModel? = null
        var musicModelList : MutableList<MusicModel> = ArrayList()
        var toolbarMusicView : View? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_menu)

        toolbarMusicView = findViewById(R.id.toolbarMusic)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_shared, R.id.nav_library
            ), drawerLayout
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        // Get texts from header and change it to whole things of user
        val header = navView.getHeaderView(0)

        //Get username and email
        val usernameText = header.findViewById<TextView>(R.id.navHeaderMenu_Username)
        val emailText = header.findViewById<TextView>(R.id.navHeaderMenu_Email)

        //Check the user instance
        auth = FirebaseAuth.getInstance()

        //Ref to the real time db
        val myRef = fbDatabase.database.getReference("users").child(auth.uid!!)

        //Getting value of data base
        val value = object : ValueEventListener{

            //If data change
            override fun onDataChange(data: DataSnapshot) {

                Log.d("VALUES", "New data: $data")
                //Get user data
                userModel = UserModel(data)

                data.child("likedMusics").children.forEach { row -> userModel!!.likedMusics.add(row.key.toString()) }
                data.child("dislikedMusics").children.forEach { row -> userModel!!.dislikedMusics.add(row.key.toString()) }

                if (data.hasChild("playedMusics")) {
                    data.child("playedMusics").children.forEach {row -> userModel!!.playedMusics.add(
                        MusicModel(row.key.toString(), Timestamp.valueOf(row.child("lastPlay").value.toString()), row.child("clicks").value.toString().toInt())
                    )}
                }


                userModel!!.playedMusics.forEach{row -> Log.d("VALUES", "Music ID: ${row.id} | Music last play: ${row.lastPlay} | Music clicks: ${row.clicks}")}
                Log.d("VALUES", "Liked musics: ${userModel!!.likedMusics.joinToString()}")

                Log.d("VALUES", "Data | Like Array: " + data.child("likedMusics").value + " | Dislike Array: " +  data.child("dislikedMusics").value)

                //Toast the username
                //Toast.makeText(baseContext, "Hey " + userModel?.username, Toast.LENGTH_SHORT).show()

                //Passing the username and email to header
                usernameText.text = userModel?.username
                emailText.text   = userModel?.email

            }

            //Cancelled
            override fun onCancelled(error: DatabaseError) {

                //Error load
                Toast.makeText(baseContext, "Error loading user data", Toast.LENGTH_SHORT).show()

            }
        }

        //Running database get values
        myRef.addListenerForSingleValueEvent(value)

        //Logout
        logout_button.setOnClickListener{

            //Get out of the firebase auth
            FirebaseAuth.getInstance().signOut()

            //Intent to login
            val intent = Intent(this, MainActivity::class.java)

            //Start the activity
            startActivity(intent)

        }

        themeChange_button.setOnClickListener {
            val mDayNightMode = if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO
            AppCompatDelegate.setDefaultNightMode(mDayNightMode)
            delegate.applyDayNight()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
