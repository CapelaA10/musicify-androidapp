package ipca.project.musicify

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import ipca.project.musicify.MenuActivity.Companion.musicModelList
import ipca.project.musicify.models.MusicModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //Variable Auth
    private lateinit var auth: FirebaseAuth

    companion object {
        lateinit var fbDatabase: DatabaseReference
    }

    //Tag of the page
    private val tag = "Login"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        //Check the user instance and init database instance
        auth = FirebaseAuth.getInstance()
        fbDatabase = FirebaseDatabase.getInstance().reference

        //Geting the inputs by find views by id
        val emailText = findViewById<TextInputLayout>(R.id.inputTextusername)
        val passwordText = findViewById<TextInputLayout>(R.id.passwordTextInputLayout)

        //Getting the email text and password
        val email = findViewById<View>(R.id.inputTextUsername) as TextInputEditText
        val password = findViewById<View>(R.id.passwordText) as TextInputEditText

        //Login btt clicked
        loginButton.setOnClickListener{

            //Value of the text email and password
            val emailTextValue    : String = email.text.toString()
            val passwordTextValue : String = password.text.toString()

            //If the value is empty if not get to the login
            if(emailTextValue.isEmpty() || passwordTextValue.isEmpty()){

                //Informing the user about the email or password empty
                Toast.makeText(baseContext, "Email empty or password", Toast.LENGTH_SHORT).show()

            }
            else {

                //Login the user
                loginEmailMethod(emailTextValue, passwordTextValue)

            }
        }

        //Register btt clicked
        registerButton.setOnClickListener{

            //Intent clean to the register activity
            val intent = Intent(this, RegisterActivity::class.java)

            //Starting the new register activity
            startActivity(intent)

        }

        //Email error off
        email.setOnClickListener{

            //Displaying the non error in the text box
            emailText.isErrorEnabled = false

        }

        //Password error off
        password.setOnClickListener{

            //Displaying the non error in the text box
            passwordText.isErrorEnabled = false

        }

        //Reset password
        textViewForgot.setOnClickListener{
            //Value of the text email
            val emailTextValue : String = email.text.toString()

            if(emailTextValue.isEmpty())
            {
                emailText.isErrorEnabled = true

                //Need email
                Toast.makeText(baseContext, "Need email", Toast.LENGTH_SHORT).show()
            }else{

                //Send password reset mail
                auth.sendPasswordResetEmail(emailTextValue).addOnCompleteListener(this) {task ->
                        if(task.isSuccessful){

                            //Toast
                            Toast.makeText(baseContext, "Password reset send email", Toast.LENGTH_SHORT).show()

                        }else{

                            //Toast
                            Toast.makeText(baseContext, "Error", Toast.LENGTH_SHORT).show()

                        }
                    }
            }
        }
    }

    //On start activity
    override fun onStart() {
        super.onStart()

        // Load musics
        loadMusics()

        // Check if user is signed in
        val currentUser = auth.currentUser

        //If the user exist
        if(currentUser != null){
            //Intent to menu
            val intent = Intent(this, MenuActivity::class.java)

            //Starting intent
            startActivity(intent)
        }
    }

    //Function to the login
    private fun loginEmailMethod(email : String, password:String){

        //Sign in firebase email
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
                //If the login was successful
                if (task.isSuccessful) {

                    // Sign in success, update UI with the signed-in user's information
                    Log.d(tag, "signInWithEmail:success")

                    //Login toast
                    Toast.makeText(baseContext, "Login Done.", Toast.LENGTH_SHORT).show()

                    //Intent to menu
                    val intent = Intent(this, MenuActivity::class.java)

                    //Starting intent
                    startActivity(intent)
                }
                else {
                    // If sign in fails, display a message to the user.
                    Log.w(tag, "signInWithEmail:failure", task.exception)

                    //Toast error login
                    Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()

                    //Updating the UI to the user
                    loginEmailError()
                }
            }
    }

    //Login error method email
    private fun loginEmailError(){

        //Geting the inputs by find views by id
        val emailText = findViewById<TextInputLayout>(R.id.inputTextusername)
        val passwordText = findViewById<TextInputLayout>(R.id.passwordTextInputLayout)

        //Display Message error
        emailText.error = "Wrong"
        passwordText.error = "Wrong"

        //Displaying the error in the text box
        emailText.isErrorEnabled    = true
        passwordText.isErrorEnabled = true

    }

    private fun loadMusics() {
        val musicListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                dataSnapshot.children.forEach {row ->
                    Log.d("VALUES", "Row: " + row)
                    if (!row.child("disabled").value.toString().toBoolean())
                        musicModelList.add(MusicModel(row))
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.d("FIREBASE", "loadPost:onCancelled", databaseError.toException())
            }
        }
        fbDatabase.database.getReference("Musicas").addListenerForSingleValueEvent(musicListener)
    }
}