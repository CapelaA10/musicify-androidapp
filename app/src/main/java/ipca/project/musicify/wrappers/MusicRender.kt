package ipca.project.musicify.wrappers

import ipca.project.musicify.models.Pattern
import ipca.project.musicify.models.Track
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.ArrayList

class MusicRender {
    var tracks: MutableList<Track> = ArrayList()
    var patterns: MutableList<Pattern> = ArrayList()

    constructor(tracks : MutableList<Track>, patterns : MutableList<Pattern>){
        this.tracks = tracks
        this.patterns = patterns
    }

    fun completeObject() : JSONObject{
        val jsonObjectTracks = JSONObject()
        val jsonObjectPatterns = JSONObject()
        val jsonArrayPattern = JSONArray()
        val jsonArrayTracks = JSONArray()

        for(i in 0 until patterns.size){
            jsonArrayPattern.put(patterns[i].toJson())
        }

        for(i in 0 until tracks.size){
            jsonArrayTracks.put(tracks[i].toJson())
        }

        jsonObjectTracks.put("tracks", jsonArrayTracks)
        jsonObjectPatterns.put("patterns", jsonArrayPattern)

        val jsonObjectFinal = JSONObject()

        jsonObjectFinal.put("tracks", jsonArrayTracks)
        jsonObjectFinal.put("patterns", jsonArrayPattern)

        return jsonObjectFinal
    }
}