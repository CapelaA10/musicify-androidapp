package ipca.project.musicify.wrappers

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.core.graphics.drawable.toBitmap
import com.google.android.material.button.MaterialButton
import ipca.project.musicify.MainActivity.Companion.fbDatabase
import ipca.project.musicify.MenuActivity
import ipca.project.musicify.R
import ipca.project.musicify.models.MusicModel
import org.jetbrains.anko.imageBitmap
import java.sql.Timestamp

object AudioManager {
    private var mediaPlayer: MediaPlayer? = null
    private val handler = Handler()
    private lateinit var runnableCode: Runnable

    fun playAudio(context: Context?, url: String?) : Boolean {
        val mediaPlayerTemp = MediaPlayer.create(context, Uri.parse(url))

        if (mediaPlayerTemp == null) {
            Toast.makeText(context, "Music isn't possible to load", Toast.LENGTH_SHORT).show()
            return false
        }

        stopAudio()
        mediaPlayer = mediaPlayerTemp

        mediaPlayer!!.setOnCompletionListener {
            stopAudio()
        }

        mediaPlayer!!.start()
        updateProgressBar()

        return true
    }

    fun pauseAudio() : Boolean {
        if (mediaPlayer != null) {
            try {
                return if (mediaPlayer!!.isPlaying) {
                    mediaPlayer!!.pause()
                    handler.removeCallbacks(runnableCode)
                    true
                } else {
                    mediaPlayer!!.start()
                    handler.post(runnableCode)
                    false
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return false
    }

    fun stopAudio() {
        if (mediaPlayer != null) {
            try {
                //mediaPlayer!!.reset()
                //mediaPlayer!!.release()
                handler.removeCallbacks(runnableCode)
                mediaPlayer!!.stop()
                mediaPlayer = null
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun updateProgressBar() {
        val progressBar = MenuActivity.toolbarMusicView!!.findViewById<ProgressBar>(R.id.musicProgressBar)
        progressBar.max = mediaPlayer!!.duration
        progressBar.progress = 0

        // Define the code block to be executed
        runnableCode = object : Runnable {
            override fun run() {
                progressBar.progress = mediaPlayer!!.currentPosition
                handler.postDelayed(this, 1000) // Repeat this the same runnable code block again another 1 seconds
            }
        }
        // Start the initial runnable task by posting through the handler
        handler.post(runnableCode)
    }

    fun toolbar(context: Context?, musicModel: MusicModel, imageView: ImageView?) {
        if (playAudio(context, musicModel.urlMusic)) {
            val toolbar = MenuActivity.toolbarMusicView!!
            toolbar.visibility = View.VISIBLE

            // Change actual image to music image was played
            val imageMusicToolbar = toolbar.findViewById<ImageView>(R.id.libraryMusicPicture)
            if (imageView != null)
                imageMusicToolbar.imageBitmap = imageView.drawable.toBitmap()
            else
                GetImageFromURL(imageMusicToolbar).execute(musicModel.urlImage)

            // Change texts
            val textMusicName = toolbar.findViewById<TextView>(R.id.textLibraryMusicName)
            val textMusicAuthor = toolbar.findViewById<TextView>(R.id.textLibraryActions)
            textMusicName.text = musicModel.name
            textMusicAuthor.text = musicModel.author

            // Button of start or pause music
            val buttonStartOrPause = toolbar.findViewById<MaterialButton>(R.id.buttonMusicStartOrPause)
            buttonStartOrPause.setOnClickListener {
                if (pauseAudio()) // If received true, is because the music was paused
                    buttonStartOrPause.setIconResource(R.drawable.ic_music_play)
                else
                    buttonStartOrPause.setIconResource(R.drawable.ic_music_pause)
            }

            // Button for close the music bar
            val buttonClear = toolbar.findViewById<MaterialButton>(R.id.buttonMusicClear)
            buttonClear.setOnClickListener {
                stopAudio()
                toolbar.visibility = View.INVISIBLE
            }

            // Update the values on firebase and local array of user
            val updateUserPlayedMusics : HashMap<String, String> = HashMap()
            Log.d("VALUES", "First ID: ${musicModel.id} | Condition: ${MenuActivity.userModel!!.playedMusics.find { it.id == musicModel.id } in MenuActivity.userModel!!.playedMusics}")

            if (MenuActivity.userModel!!.playedMusics.find { it.id == musicModel.id } in MenuActivity.userModel!!.playedMusics) {
                MenuActivity.userModel!!.playedMusics.forEach { row ->
                    row.takeIf { it.id == musicModel.id }?.let {
                        it.clicks = it.clicks!!.plus(1)
                        it.lastPlay = Timestamp(System.currentTimeMillis())

                        updateUserPlayedMusics["clicks"] = it.clicks.toString()
                        updateUserPlayedMusics["lastPlay"] = it.lastPlay.toString()

                        Log.d("VALUES-ENTRY", "Entrei #1")
                    }
                }
            } else {
                val newMusicModel = MusicModel(musicModel.id.toString(), Timestamp(System.currentTimeMillis()), 1)
                MenuActivity.userModel!!.playedMusics.add(newMusicModel)

                updateUserPlayedMusics["clicks"] = newMusicModel.clicks.toString()
                updateUserPlayedMusics["lastPlay"] = newMusicModel.lastPlay.toString()

                Log.d("VALUES-ENTRY", "Entrei #2")
            }

            fbDatabase.child("users").child(MenuActivity.userModel!!.id.toString()).child("playedMusics").child(musicModel.id.toString()).setValue(updateUserPlayedMusics)
        }
    }
}