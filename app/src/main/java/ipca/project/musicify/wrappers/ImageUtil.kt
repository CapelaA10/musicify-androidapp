package ipca.project.musicify.wrappers

import android.graphics.*
import android.os.AsyncTask
import android.widget.ImageView
import org.jetbrains.anko.imageBitmap
import java.net.URL
import kotlin.random.Random

// Thread to get image from a URL
class GetImageFromURL(imageReceived: ImageView?) : AsyncTask<String?, Void?, Bitmap?>() {
    private var bitmapImage: ImageView? = imageReceived

    override fun doInBackground(vararg url: String?) : Bitmap? {
        return try {
            val buffer = URL(url[0]).openStream()
            BitmapFactory.decodeStream(buffer)
        } catch (e: Exception) {
            generateGradientImage(100f, 100f)
            //Log.d("ERROR", e.message)
            //e.printStackTrace()
        }
    }

    override fun onPostExecute(result: Bitmap?) {
        bitmapImage!!.imageBitmap = result
    }

    fun generateGradientImage(width : Float, height : Float) : Bitmap {
        val colors = intArrayOf(Color.rgb(Random.nextInt(0, 255), Random.nextInt(0, 255), Random.nextInt(0, 255)), Color.rgb(Random.nextInt(0, 255), Random.nextInt(0, 255), Random.nextInt(0, 255)), Color.rgb(Random.nextInt(0, 255), Random.nextInt(0, 255), Random.nextInt(0, 255)))
        val positions = floatArrayOf(0f, 5f / 360f, 1f)

        // Create gradient
        val p = Paint()
        p.isDither = true
        p.shader = LinearGradient(0f, 0f, width, height, colors, positions, Shader.TileMode.MIRROR)

        // Create bitmap and insert the paint
        val bmp = Bitmap.createBitmap(width.toInt(), height.toInt(), Bitmap.Config.ARGB_8888)
        Canvas(bmp).drawPaint(p)

        return bmp
    }
}
