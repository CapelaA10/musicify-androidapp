package ipca.project.musicify

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import ipca.project.musicify.models.UserModel
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    //Auth
    private lateinit var auth: FirebaseAuth

    //Account
    private var email           = " "
    private var password        = " "
    private var confirmPassword = " "

    //Profile
    private var username        = " "
    private var firstName       = " "
    private var secondName      = " "

    //TAG page
    private var tag = "REGISTER"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        //Btt Register
        val registerBtt = findViewById<Button>(R.id.registerButton)

        //Var to verification
        var ver : Boolean

        registerBtt.setOnClickListener{

            //Account
            email  = inputEmailText.text.toString()
            password = inputPasswordText.text.toString()
            confirmPassword = inputPasswordConfirmText.text.toString()

            //Profile
            username = inputTextUsername.text.toString()
            firstName = inputFirstNameText.text.toString()
            secondName = inputSecondNameText.text.toString()

            //UserModel Info not empty
            ver = verifyUserInfo(username, email, password)

            //If to ver
            if(!ver){

                //Error
                Toast.makeText(baseContext, "Not all info completed",
                    Toast.LENGTH_SHORT).show()

            }
            else {

                //Ver to check password
                ver = checkThePasswordIsEqual(password, confirmPassword)

                //If ver
                if(!ver){

                    //Error
                    Toast.makeText(baseContext, "Password Not Equal",
                        Toast.LENGTH_SHORT).show()

                }
                else{

                    //Register
                    registerUser(email, password)

                }
            }

        }
    }

    //Register the user
    private fun registerUser(email : String , password : String){

        //Function firebase to register the user
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {

                    // Sign in success, update UI with the signed-in user's information
                    Log.d(tag, "createUserWithEmail:success")
                    Toast.makeText(baseContext, "Register Done.",
                        Toast.LENGTH_SHORT).show()

                    //Get user to the db
                    val user = auth.currentUser
                    registerInTheDb(username, firstName, secondName, email, user)

                    val intent = Intent(this, MenuActivity::class.java)

                    //Starting the new register activity
                    startActivity(intent)

                } else {

                    // If sign in fails, display a message to the user.
                    Log.w(tag, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()

                }
            }
    }

    private fun verifyUserInfo(username : String, email : String, password : String) : Boolean{
        return !(username.isEmpty() || email.isEmpty() || password.isEmpty())
    }

    private fun checkThePasswordIsEqual(password: String, confirmPassword:String) : Boolean{
        return password == confirmPassword
    }

    public override fun onStart() {
        super.onStart()
    }

    private fun registerInTheDb(username : String?, firstName : String?, secondName : String?, email : String, user : FirebaseUser?){

        //If the user name is empty
        if(firstName!!.isEmpty() || secondName!!.isEmpty()){

            //Creating the new user
            val newUser = UserModel(user!!.uid, username, email ,"null", "null", ArrayList(), ArrayList(), ArrayList())

            //Database instance
            val database = FirebaseDatabase.getInstance()

            //Ref/path
            val myRef = database.getReference("users").child(user!!.uid)

            //Set the value
            myRef.setValue(newUser).addOnSuccessListener {

                //Done
                Toast.makeText(baseContext, "Register Done",
                    Toast.LENGTH_SHORT).show()

            }
                .addOnFailureListener {

                    //Error
                    Toast.makeText(baseContext, "Register Db Not Done",
                        Toast.LENGTH_SHORT).show()

                }

        }else{

            //Creating the new user
            val newUser = UserModel(user!!.uid, username, email, firstName, secondName, ArrayList(), ArrayList(), ArrayList())

            //Database instace
            val database = FirebaseDatabase.getInstance()

            //Ref/path
            val myRef   = database.getReference("users").child(user!!.uid)

            //Set the value
            myRef.setValue(newUser).addOnSuccessListener {
                Toast.makeText(baseContext, "Register Done",
                    Toast.LENGTH_SHORT).show()
            }
                .addOnFailureListener {
                    Toast.makeText(baseContext, "Register Db Not Done",
                        Toast.LENGTH_SHORT).show()
                }


        }

        //Send verification email
        user.sendEmailVerification()
            .addOnCompleteListener(this) { task ->

                if (task.isSuccessful) {

                    //Say to the user verify email
                    Toast.makeText(baseContext,
                        "Verification email sent to ${user.email} ",
                        Toast.LENGTH_SHORT).show()

                } else {

                    //If error
                    Log.e(tag, "sendEmailVerification", task.exception)

                    //Toast to user
                    Toast.makeText(baseContext,
                        "Failed to send verification email.",
                        Toast.LENGTH_SHORT).show()

                }
            }
    }
}
