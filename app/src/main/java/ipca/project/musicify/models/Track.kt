package ipca.project.musicify.models

import org.json.JSONObject

class Track {

    var volume  : Int = 0

    var name    : String? = null
    var active  : Boolean = false

    //Full constructor
    constructor(name : String?, volume : Int, active : Boolean){

        this.volume.coerceIn(0, 100)

        this.name   = name
        this.active = active
        this.volume = volume
    }

    fun toJson() : JSONObject{
        val jsonObject = JSONObject()

        jsonObject.put("volume", this.volume)
        jsonObject.put("name", this.name)
        jsonObject.put("active", this.active)

        return jsonObject
    }
}