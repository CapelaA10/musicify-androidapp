package ipca.project.musicify.models

import com.google.firebase.database.DataSnapshot
import java.io.Serializable
import java.sql.Timestamp
import java.util.*

class MusicModel : Serializable {
    var id : String? = null
    var authorId : String? = null
    var author : String? = null
    var name : String? = null

    var urlImage : String? = null
    var urlMusic : String? = null

    var likes : Int? = 0
    var dislikes : Int? = 0

    var share : Boolean? = false
    var private : Boolean? = true
    var sharedWith : MutableList<String>? = arrayListOf()

    var disabled : Boolean? = false
    var created : String? = Timestamp(System.currentTimeMillis()).toString()

    // For played musics
    var lastPlay : Date? = null
    var clicks : Int? = null

    constructor(authorId : String, author : String, name : String, image : String, music : String, likes : Int, dislikes : Int, shared : Boolean, private : Boolean, sharedWith : MutableList<String>, disabled : Boolean, created : String) {
        this.authorId = authorId
        this.author = author
        this.name = name

        this.urlImage = image
        this.urlMusic = music

        this.likes = likes
        this.dislikes = dislikes

        this.share = shared
        this.private = private
        this.sharedWith = sharedWith

        this.disabled = disabled
        this.created = created
    }

    constructor(dataSnapshot: DataSnapshot){
        this.id         = dataSnapshot.key!!
        this.authorId   = dataSnapshot.child("authorId").value.toString()
        this.author     = dataSnapshot.child("author").value.toString()
        this.name       = dataSnapshot.child("name").value.toString()

        this.urlImage   = dataSnapshot.child("urlImage").value.toString()
        this.urlMusic   = dataSnapshot.child("urlMusic").value.toString()

        this.likes      = dataSnapshot.child("likes").value.toString().toInt()
        this.dislikes   = dataSnapshot.child("dislikes").value.toString().toInt()

        this.share      = dataSnapshot.child("share").value.toString().toBoolean()
        this.private    = dataSnapshot.child("private").value.toString().toBoolean()

        this.created    = dataSnapshot.child("created").value.toString()
    }

    constructor(id : String, lastPlay : Date?, clicks : Int?) {
        this.id = id
        this.lastPlay = lastPlay
        this.clicks = clicks
    }
}

