package ipca.project.musicify.models

import org.json.JSONObject
import java.net.URLEncoder
import java.util.concurrent.TimeUnit

class Pattern {

    var name        : String? = null
    var type        : String? = null
    var initialTime : Float = 0.0F
    var finalTime   : Float = 0.0F
    var linkMusicMp3 : String? = null

    var timeInit : String? = null
    var timeEnd : String? = null
    var maxS : Float = 0f

    constructor(name : String, type : String?){

        this.name = name
        this.type = type
    }

    fun setInitFinal(initialTime : Float, finalTime : Float){

        this.initialTime = initialTime
        this.finalTime   = finalTime
    }

    fun toJson() : JSONObject{
        val jsonObject = JSONObject()

        convertFloatToTime()

        /*var index1 = linkMusicMp3?.indexOf("%")
        var encondedLinkFirst = index1?.let { linkMusicMp3!!.substring(0, it) }
        index1!!.plus(2)
        var encondedLinkSecound = index1?.let { linkMusicMp3!!.substring(it) }*/



        var link = ""
        var encondedLinkFirst = ""
        var encondedLinkSecound = ""
        var countUnits = 0
        var inTheMiddle = false

        for (index in 0 until  linkMusicMp3!!.length){
            if (inTheMiddle){
                if (countUnits <= 0)
                    link += linkMusicMp3!![index]

                countUnits--
            }
            else if (linkMusicMp3!![index] != '%' && !inTheMiddle){
                link += linkMusicMp3!![index]
            }
            else{
                encondedLinkFirst = link

                link = ""
                countUnits = 2
                inTheMiddle = true
            }
        }

        encondedLinkSecound = link

        encondedLinkFirst = URLEncoder.encode(encondedLinkFirst, "UTF-8")
        encondedLinkSecound = URLEncoder.encode(encondedLinkSecound, "UTF-8")

        jsonObject.put("trackName", this.name)
        jsonObject.put("initTime", this.timeInit)
        jsonObject.put("cutTimeEnd", this.timeEnd)
        jsonObject.put("source1", encondedLinkFirst)
        jsonObject.put("source2", encondedLinkSecound)

        return jsonObject
    }

    private fun convertFloatToTime(){
        val maxF = 180000
        val minF = 0

        val minutesI = ((initialTime*maxF)/ maxS) /// 1000 / 60
        val secondsI = ((initialTime*maxF)/ maxS) /// 1000 % 60

        var minutInitI = TimeUnit.MILLISECONDS.toMinutes(minutesI.toLong())
        var secInitI = TimeUnit.MILLISECONDS.toSeconds(secondsI.toLong())

        val minutesE = ((finalTime*maxF)/ maxS) /// 1000 / 60
        val secondsE =((finalTime*maxF)/ maxS) /// 1000 % 60

        val minutFinalE = TimeUnit.MILLISECONDS.toMinutes(minutesE.toLong())
        val secFinalE = TimeUnit.MILLISECONDS.toSeconds(secondsE.toLong())

        timeInit = "00:" + secCut(minutInitI, secInitI)
        timeEnd = "00:" + secCut(minutFinalE, secFinalE)
    }

    fun addZero(time : Long): String{
        var result = ""

        if (time < 10){
            result= "0$time"
        }else{
            result = time.toString()
        }

        return result
    }

    fun secCut(min : Long, sec : Long) : String{
        var minT = min
        var secT = sec
        var result = ""

        if(sec>59){
            minT += 1
            secT = 0
        }

        result=addZero(minT)+ ":" + addZero(secT)

        return result
    }
}