package ipca.project.musicify.models

import com.google.firebase.database.DataSnapshot

class UserModel {
    var id : String? = null
    var username : String? = null
    var email: String? = null
    var firstName : String? = null
    var lastName : String? = null

    var likedMusics : MutableList<String>
    var dislikedMusics : MutableList<String>

    var playedMusics : MutableList<MusicModel>

    constructor(id : String?, username : String?, email : String?, firstName: String?, secondName: String?, likedMusics: MutableList<String>, dislikedMusics : MutableList<String>, playedMusics : MutableList<MusicModel>){
        this.id = id
        this.username = username
        this.email = email
        this.firstName = firstName
        this.lastName = secondName
        this.likedMusics = likedMusics
        this.dislikedMusics = dislikedMusics
        this.playedMusics = playedMusics
    }

    constructor(dataSnapshot: DataSnapshot){
        this.id             = dataSnapshot.key
        this.username       = dataSnapshot.child("username"  ).value.toString()
        this.email          = dataSnapshot.child("email"     ).value.toString()
        this.firstName      = dataSnapshot.child("firstName" ).value.toString()
        this.lastName       = dataSnapshot.child("lastName"  ).value.toString()
        this.likedMusics    = arrayListOf()//if (dataSnapshot.hasChild("likedMusics")) dataSnapshot.child("likedMusics").value.toString().substring(1, dataSnapshot.child("likedMusics").value.toString().length - 1).split(", ").toMutableList() else arrayListOf() //if (dataSnapshot.hasChild("likedMusics")) arrayListOf(dataSnapshot.child("likedMusics").value.toString().substring(1, dataSnapshot.child("likedMusics").value.toString().length - 1)) else arrayListOf()
        this.dislikedMusics = arrayListOf()//if (dataSnapshot.hasChild("dislikedMusics")) dataSnapshot.child("dislikedMusics").value.toString().substring(1, dataSnapshot.child("dislikedMusics").value.toString().length - 1).split(", ").toMutableList() else arrayListOf() //if (dataSnapshot.hasChild("dislikedMusics")) arrayListOf(dataSnapshot.child("dislikedMusics").value.toString().substring(1, dataSnapshot.child("dislikedMusics").value.toString().length - 1)) else arrayListOf()
        this.playedMusics   = arrayListOf()
    }
}